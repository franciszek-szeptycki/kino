from django.db import models
from django.utils.text import slugify
from django.db.models.signals import post_delete
from django.dispatch import receiver
import os



class Category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = "Categories"



class Movie(models.Model):
    title = models.CharField(max_length=100)

    author = models.CharField(max_length=100)
    image = models.ImageField(upload_to='images/', blank=True, null=True)
    categories = models.ManyToManyField(Category)
    description = models.TextField()
    trailer = models.URLField()
    
    expired = models.BooleanField(default=False)

    pegi = models.IntegerField(
        choices=[
            (3, "3"),
            (7, "7"),
            (12, "12"),
            (16, "16"),
            (18, "18"),
        ], default=3
    )

    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name_plural = "Movies"


@receiver(post_delete, sender=Movie)
def delete_file_on_post_delete(sender, instance, **kwargs):
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)