from django.shortcuts import render
from .models import Movie


def home(request):
    
    movies = Movie.objects.all()

    return render(request, 'home.html', {'movies': movies})


def movie(request, movie_id):
    
    movie = Movie.objects.get(pk=movie_id)

    return render(request, 'movie.html', {'movie': movie})