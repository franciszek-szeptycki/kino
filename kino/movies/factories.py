from django.utils.text import slugify
import factory
from faker import Faker
from .models import Category, Movie
import random

fake = Faker()

class CategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Category

    name = factory.LazyAttribute(lambda _: fake.word())

class MovieFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Movie

    title = factory.LazyAttribute(lambda _: fake.sentence(nb_words=3))

    author = factory.LazyAttribute(lambda _: fake.name())
    image = factory.django.ImageField(width=1024, height=768)
    description = factory.LazyAttribute(lambda _: fake.paragraph())
    trailer = factory.LazyAttribute(lambda _: 'https://www.youtube.com/watch?v=dQw4w9WgXcQ') # Rick Astley - Never Gonna Give You Up

    expired = False
    pegi = random.choice([3, 7, 12, 16, 18])

    @factory.post_generation
    def categories(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for category in extracted:
                self.categories.add(category)
