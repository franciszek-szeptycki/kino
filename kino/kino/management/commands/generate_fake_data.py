from django.core.management.base import BaseCommand
from django.apps import apps
from movies.factories import CategoryFactory, MovieFactory
from kino.management.commands.seed_admin import Command as SeedAdminCommand

class Command(BaseCommand):
    help = 'Generates fake data using factory boy'

    def handle(self, *args, **kwargs):

        self._clear_data()

        CategoryFactory.create_batch(5)
        MovieFactory.create_batch(10)

        SeedAdminCommand().handle()

        self.stdout.write(self.style.SUCCESS('Successfully generated fake data'))

    def _clear_data(self):
        # Get all models from the installed apps
        models = apps.get_models()

        # Delete all data from each model
        for model in models:
            model.objects.all().delete()