import os
from dotenv import dotenv_values
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

env_values = dotenv_values(".env.example")

superuser_username = env_values.get("ADMIN_USERNAME", 'admin')
superuser_email = env_values.get("ADMIN_EMAIL", '')
superuser_password = env_values.get("ADMIN_PASSWORD", 'admin')

class Command(BaseCommand):
    help = 'Seed data for initial admin account'

    def handle(self, *args, **kwargs):
        User.objects.create_superuser(superuser_username, superuser_email, superuser_password)
        self.stdout.write(self.style.SUCCESS(f'Superuser created with username: {superuser_username} and email: {superuser_email}'))
